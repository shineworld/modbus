#ifndef __DATABASE_H
#define __DATABASE_H


#include "config.h"


/*!
    \fn int database_initialise( void * Context )
    \brief Initialise user database API components
    \param Context Pointer to user-supplied contextual data structure
    \returns Returns zero on success, non-zero on error
*/

int database_initialise( void * Context );

/*!
    \fn void database_destroy( void * Context )
    \brief Release resources and clean-up user database components
    \param Context Pointer to user-supplied contextual data structure
    \returns No return value
*/

void database_destroy( void * Context );

#if defined( CONFIG_SUPPORT_FC_READ_COILS )

/*!
    \fn int database_checkCoils( void * Context, unsigned int Start, unsigned int Count )
    \brief Validates range of coil registers received in Modbus request message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First coil register in range
    \param Count Number of coil registers in range
    \returns Returns zero on success, non-zero on error
*/

int database_checkCoils( void * Context, unsigned int Start, unsigned int Count );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )

/*!
    \fn int database_checkDiscreteInputs( void * Context, unsigned int Start, unsigned int Count )
    \brief Validates range of discrete input registers received in Modbus request message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First discrete input register in range
    \param Count Number of discrete input registers in range
    \returns Returns zero on success, non-zero on error
*/

int database_checkDiscreteInputs( void * Context, unsigned int Start, unsigned int Count );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS ) | \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

/*!
    \fn int database_checkHoldingRegisters( void * Context, unsigned int Start, unsigned int Count )
    \brief Validates range of holding registers received in Modbus request message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First holding register in range
    \param Count Number of holding registers in range
    \returns Returns zero on success, non-zero on error
*/

int database_checkHoldingRegisters( void * Context, unsigned int Start, unsigned int Count );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )

/*!
    \fn int database_checkInputRegisters( void * Context, unsigned int Start, unsigned int Count )
    \brief Validates range of input registers received in Modbus request message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First input register in range
    \param Count Number of input registers in range
    \returns Returns zero on success, non-zero on error
*/

int database_checkInputRegisters( void * Context, unsigned int Start, unsigned int Count );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_COILS )

/*!
    \fn int database_readCoils( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Reads coil register values for population of Modbus response message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First coil register in range
    \param Count Number of coil registers in range
    \param Data Pointer to data buffer where coil register values should be populated
    \returns Returns zero on success, non-zero on error
*/

int database_readCoils( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )

/*!
    \fn int database_readDiscreteInputs( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Reads discrete input register values for population of Modbus response message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First discrete input register in range
    \param Count Number of discrete input registers in range
    \param Data Pointer to data buffer where discrete input register values should be populated
    \returns Returns zero on success, non-zero on error
*/

int database_readDiscreteInputs( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_EXCEPTION_STATUS )

/*!
    \fn int database_readExceptionStatus( void * Context, unsigned char * Status )
    \brief Reads exception status outputs for population of Modbus response message
    \param Context Pointer to user-supplied contextual data structure
    \param Status Pointer to buffer byte where exception status should be populated
    \returns Returns zero on success, non-zero on error
*/

int database_readExceptionStatus( void * Context, unsigned char * Status );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS ) | \
        defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER ) | \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

/*!
    \fn int database_readHoldingRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Reads holding register values for population of Modbus response message
    \param Context Pointer to user-supplied contextual data structure
    \param Start First holding register in range
    \param Count Number of holding registers in range
    \param Data Pointer to data buffer where holding register values should be populated
    \returns Returns zero on success, non-zero on error
*/

int database_readHoldingRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )

/*!
    \fn int database_readInputRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Reads input register values for populate of Modbus response message
    \param Context Pointer to user-supplied contexutal data structure
    \param Start First input register in range
    \param Count Number of input registers in range
    \param Data Pointer to data buffer where input register values should be populated
    \returns Returns zero on success, non-zero on error
*/

int database_readInputRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL ) | \
        defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )

/*!
    \fn int database_writeCoils( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Writes coil register values following Modbus request
    \param Context Pointer to user-supplied contextual data structure
    \param Start First coil register in range
    \param Count Number of coil registers in range
    \param Data Pointer to data buffer with coil register values
    \returns Returns zero on success, non-zero on error
*/

int database_writeCoils( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER ) | \
        defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS ) | \
        defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER ) | \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

/*!
    \fn int database_writeHoldingRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data )
    \brief Writes holding register values following Modbus request
    \param Context Pointer to user-supplied contextual data structure
    \param Start First holding register in range
    \param Count Number of holding registers in range
    \param Data Pointer to data buffer with holding register values
    \returns Returns zero on success, non-zero on error
*/

int database_writeHoldingRegisters( void * Context, unsigned int Start, unsigned int Count, char * Data );

#endif

#endif

