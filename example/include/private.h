#ifndef __PRIVATE_H
#define __PRIVATE_H


#include "app.h"
#include "modbus.h"
#include "target.h"


/*!
	This header file is intended to provide the data structures which serve as
	the glue between the example application and the Modbus protocol source code
	library.
*/


typedef struct _PRIVATE_TIMER
{
    int Id;

    TARGET_TIMER_CALLBACK Callback;

    void * Parameter;
}
PRIVATE_TIMER;


/*!
    \struct PRIVATE_DATABASE
    \brief Private database data structure
*/

typedef struct _PRIVATE_DATABASE
{
    uint8_t Coil[2];

    uint8_t Discrete[2];

    uint16_t Holding[10];

    uint16_t Input[10];

    void * Target;
}
PRIVATE_DATABASE;


/*!
	\struct PRIVATE_INTERFACE
	\brief Private interface data structure
*/

typedef struct _PRIVATE_INTERFACE
{
	MODBUS_CONTEXT * Context;

	MODBUS_INTERFACE * Interface;

	MODBUS_INTERFACE_CONFIG Config;


	/*!
		The Address and Port members of this data structure are intended to hold
		the local IPv4 address and UDP port on which the example application
		creates a listening TCP socket.  The corresponding file descriptor created
		for these Modbus protocol operations is stored in the Socket member of this
		data structure.
	*/

    const char * Address;

	unsigned short Port;

    int Socket;
}
PRIVATE_INTERFACE;


/*!
    \struct PRIVATE_SESSION
    \brief Private session data structure
*/

typedef struct _PRIVATE_SESSION
{
	MODBUS_SESSION * Session;

	MODBUS_SESSION_CONFIG Config;

	PRIVATE_INTERFACE * Interface;

    int Socket;


    /*!
    	The Callback and Parameter members of this data structure are intended to
    	hold function callback and contextual parameters employed for the
    	injection of received data bytes into the embedded protocol library for
    	processing.
    */

	TARGET_RECEIVE_CALLBACK Callback;

	void * Parameter;
}
PRIVATE_SESSION;


typedef struct _PRIVATE_CONTEXT
{
    APP App;

    PRIVATE_INTERFACE Interface;

    PRIVATE_DATABASE Database;
}
PRIVATE_CONTEXT;


#endif
