#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

#include "app.h"
#include "log.h"
#include "modbus.h"
#include "private.h"
#include "target.h"


#define __UNUSED(x)     ( ( void ) ( (x) == (x) ) )


static void _target_acceptCallback( APP * App, int Handle, void * Parameter );

static void _target_handleCallback( APP * App, int Handle, void * Parameter );

static void _target_timerCallback( APP * App, int Timer, void * Parameter );


void
_target_acceptCallback( APP * App, int Handle, void * Parameter )
{
    PRIVATE_SESSION *pSession;
    PRIVATE_INTERFACE *pInterface;
    struct sockaddr_in stAddress;
    socklen_t uSize;
    int nSocket;


    pInterface = ( PRIVATE_INTERFACE * ) Parameter;
    /* assert( pInterface != NULL ); */
    /* assert( pInterface->Socket == Handle ); */

    uSize = sizeof( stAddress );
    if( ( nSocket = accept( Handle, ( struct sockaddr * ) &stAddress, &uSize ) ) < 0 )
    {
        return /* -errno */;
    }

    if( ( pSession = target_alloc( App, sizeof( *pSession ) ) ) == NULL )
    {
        close( nSocket );
        return /* -errno */;
    }
    ( void ) memset( pSession, 0, sizeof( *pSession ) );
    pSession->Interface = pInterface;
    pSession->Socket = nSocket;
    pSession->Config.Address = 1;
    pSession->Config.Type = TYPE_SLAVE;

    if( ( pSession->Session = modbus_openSession( pInterface->Interface, &pSession->Config, pSession ) ) == NULL )
    {
    	close( nSocket );
    	return /* -errno */;
    }

    if( app_addReadCallback( App, pSession->Socket, _target_handleCallback, pSession ) != 0 )
    {
    	modbus_closeSession( pSession->Session );

    	close( pSession->Socket );
        target_free( App, pSession );

        return /* -errno */;
    }

    LOG_INFO( "Accepted connection from %s", inet_ntoa( stAddress.sin_addr ) );
}


void
_target_handleCallback( APP * App, int Handle, void * Parameter )
{
    PRIVATE_SESSION *pSession;


    __UNUSED( App );
    __UNUSED( Handle );

    pSession = ( PRIVATE_SESSION * ) Parameter;
    /* assert( pSession != NULL ); */
    /* assert( pSession->Socket == Handle ); */
    /* assert( pSession->Callback != NULL ) */

    pSession->Callback( pSession->Parameter );
}


void
_target_timerCallback( APP * App, int Timer, void * Parameter )
{
    PRIVATE_TIMER *pTimer;


    pTimer = ( PRIVATE_TIMER * ) Parameter;
    /* assert( pTimer != NULL ); */
    /* assert( pTimer->Id == Timer ); */

    pTimer->Callback( Timer, Parameter );
    target_free( App, pTimer );
}


int
target_initialise( void * Target )
{
    log_initialise( Target );

    return 0;
}


void *
target_alloc( void * Target, unsigned int Bytes )
{
	__UNUSED( Target );

    return calloc( 1, Bytes );
}


void
target_free( void * Target, void * Data )
{
	__UNUSED( Target );

    if( Data != NULL )
    {
        free( Data );
    }
}


uint32_t
target_getMillisecondCounter( void * Target )
{
    struct timeval stTv;


    target_getSystemTime( Target, &stTv );
    return ( uint32_t ) ( ( stTv.tv_sec * 1000 ) + ( stTv.tv_usec / 1000 ) );
}


void
target_getSystemTime( void * Target, struct timeval * Time )
{
    struct timespec stTp;


    __UNUSED( Target );

    ( void ) clock_gettime( CLOCK_MONOTONIC, &stTp );
    Time->tv_sec = stTp.tv_sec;
    Time->tv_usec = stTp.tv_nsec / 1000;
}


int
target_startTimer( void * Target, unsigned long Timeout, TARGET_TIMER_CALLBACK Callback, void * Parameter )
{
	APP *pApp;
    PRIVATE_TIMER *pTimer;


    pApp = ( APP * ) Target;
    /* assert( pApp != NULL ); */

    if( ( pTimer = ( PRIVATE_TIMER * ) target_alloc( pApp, sizeof( *pTimer ) ) ) == NULL )
    {
        return -ENOMEM;
    }
    pTimer->Callback = Callback;
    pTimer->Parameter = Parameter;
    pTimer->Id = app_addTimer( pApp, Timeout, _target_timerCallback, pTimer );

    return pTimer->Id;
}


void
target_stopTimer( void * Target, int Timer )
{
	APP *pApp;
    PRIVATE_TIMER *pTimer;


    pApp = ( APP * ) Target;
    /* assert( pApp != NULL ); */

    pTimer = NULL;

    if( app_deleteTimer( pApp, Timer, ( void ** ) pTimer ) == 0 )
    {
        /* assert( pTimer != NULL ); */
        /* assert( pTimer->Id == Timer ); */
        target_free( pApp, pTimer );
    }
}


int
target_adjustTimer( void * Target, int Timer, unsigned long Timeout )
{
	APP * pApp;


	pApp = ( APP * ) Target;
	/* assert( pApp != NULL ); */

    return app_adjustTimer( pApp, Timer, Timeout );
}


int
target_openInterface( void * Target, void * Interface )
{
	APP *pApp;
    PRIVATE_INTERFACE *pInterface;
    struct sockaddr_in stAddress;
    int nResult;


    pApp = ( APP * ) Target;
    /* assert( pApp != NULL ); */
    pInterface = ( PRIVATE_INTERFACE * ) Interface;
    /* assert( pInterface != NULL ); */

    ( void ) memset( &stAddress, 0, sizeof( stAddress ) );
    stAddress.sin_family = AF_INET;
    stAddress.sin_addr.s_addr = inet_addr( pInterface->Address );
    stAddress.sin_port = htons( pInterface->Port );

    if( ( pInterface->Socket = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
    {
        close( pInterface->Socket );
        return -errno;
    }

    if( bind( pInterface->Socket, ( struct sockaddr * ) &stAddress, sizeof( stAddress ) ) < 0 )
    {
        close( pInterface->Socket );
        return -errno;
    }

    if( listen( pInterface->Socket, 10 ) < 0 )
    {
        close( pInterface->Socket );
        return -errno;
    }

    if( ( nResult = app_addReadCallback( pApp, pInterface->Socket, _target_acceptCallback, pInterface ) ) != 0 )
    {
        close( pInterface->Socket );
        return nResult;
    }

    LOG_INFO( "Listening for Modbus connections on port %u", pInterface->Port );

    return 0;
}


void
target_closeInterface( void * Target, void * Interface )
{
	APP * pApp;
    PRIVATE_INTERFACE *pInterface;
    void *pData;


    pApp = ( APP * ) Target;
    /* assert( pApp != NULL ); */
    pInterface = ( PRIVATE_INTERFACE * ) Interface;
    /* assert( pInterface != NULL ); */

    if( app_deleteReadCallback( pApp, pInterface->Socket, &pData ) == 0 )
    {
		/* assert( pData == pInterface ); */
		close( pInterface->Socket );
    }
}


int
target_openSession( void * Target, TARGET_RECEIVE_CALLBACK Callback, void * Parameter, void * Context )
{
	PRIVATE_SESSION *pSession;


    __UNUSED( Target );

	pSession = ( PRIVATE_SESSION * ) Context;
	/* assert( pSession != NULL ); */
	pSession->Callback = Callback;
	pSession->Parameter = Parameter;

	return 0;
}


void
target_closeSession( void * Target, void * Context )
{
	APP *pApp;
	PRIVATE_SESSION *pSession;
	void *pData;


    pApp = ( APP * ) Target;
    /* assert( pApp != NULL ); */
	pSession = ( PRIVATE_SESSION * ) Context;
	/* assert( pSession != NULL ); */

	if( app_deleteReadCallback( pApp, pSession->Socket, &pData ) == 0 )
	{
		/* assert( pData == pSession ); */
		close( pSession->Socket );

		target_free( pApp, pSession );
	}
}


/*!
	\fn int target_receiveBytes( void * Target, char * Buffer, unsigned int Length, void * Context )
	\brief Target interface callback function to receive data associated with a protocol session
 	\param Target Pointer to target layer contextual data structure
 	\param Buffer Pointer to receive data buffer
 	\param Length Maximum size of receive data buffer
	\param Context Pointer to session contextual data structure
	\return Returns number of data bytes received and populated within receive data buffer

	The target layer contextual data structure supplied to this function is that
	supplied to protocol library initialisation function.  Similarly, the session
	contextual data structure supplied to this function is that provided to the
	protocol session initialisation function.
*/

int
target_receiveBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
{
    PRIVATE_SESSION *pSession;
    int nResult;


    __UNUSED( Target );

    pSession = ( PRIVATE_SESSION * ) Context;
    /* assert( pSession != NULL ); */
    /* assert( pSession->Session != NULL ); */

    if( ( nResult = read( pSession->Socket, Buffer, Length ) ) < 0 )
    {
        return -errno;
    }
    else if( nResult == 0 )
    {
    	modbus_closeSession( pSession->Session );
    }

    return nResult;
}


/*!
    \fn int target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
    \brief Target interface callback function to transmit data associated with a protocol session
    \param Target Pointer to target layer contextual data structure
    \param Buffer Pointer to transmit data buffer
    \param Length Number of data bytes to transmit
    \param Context Pointer to session contextual data structure
    \return Returns number of data bytes transmitted
*/

int
target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
{
    PRIVATE_SESSION *pSession;
    int nResult;


    __UNUSED( Target );

    pSession = ( PRIVATE_SESSION * ) Context;
    /* assert( pSession != NULL ); */
    /* assert( pSession->Session != NULL ); */

    if( ( nResult = write( pSession->Socket, Buffer, Length ) ) <= 0 )
    {
        modbus_closeSession( pSession->Session );
    }

    return nResult;
}


uint8_t
target_getB8( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint8_t ) *Data;
}


uint16_t
target_getB16( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint16_t ) ( ( ( Data[0] << 8 ) & 0xff00 ) |
            ( Data[1] & 0x00ff ) );
}


uint32_t
target_getB32( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint32_t ) ( ( ( Data[0] << 24 ) & 0xff000000 ) |
            ( ( Data[1] << 16 ) & 0x00ff0000 ) |
            ( ( Data[2] << 8 ) & 0x0000ff00 ) |
            ( Data[3] & 0x000000ff ) );
}


uint64_t
target_getB64( void * Target, unsigned char * Data )
{
	__UNUSED( Target );
    __UNUSED( Data );

    /* assert( Data != NULL ); */
    return ( uint64_t ) 0;
}


void 
target_setB8( void * Target, unsigned char * Data, uint8_t Value )
{
    __UNUSED( Target );

    Data[0] = Value;
}


void
target_setB16( void * Target, unsigned char * Data, uint16_t Value )
{
    __UNUSED( Target );

    /* assert( Data != NULL ); */
    Data[0] = ( ( Value >> 8 ) & 0xff );
    Data[1] = ( Value & 0xff );
}


void
target_setB32( void * Target, unsigned char * Data, uint32_t Value )
{
    __UNUSED( Target );

    /* assert( Data != NULL ); */
    Data[0] = ( ( Value >> 24 ) & 0xff );
    Data[1] = ( ( Value >> 16 ) & 0xff );
    Data[2] = ( ( Value >> 8 ) & 0xff );
    Data[3] = ( Value & 0xff );
}


void
target_setB64( void * Target, unsigned char * Data, uint64_t Value )
{
    __UNUSED( Target );
    __UNUSED( Data );
    __UNUSED( Value );
}

