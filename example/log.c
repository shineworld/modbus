#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <limits.h>

#include "log.h"
#include "target.h"


static const char * _szLevel[] =
{
    "PANIC",
    "ALERT",
    "CRITICAL",
    "ERROR",
    "WARNING",
    "NOTICE",
    "INFO",
    "DEBUG",
};


static uint32_t _uStart = 0;

static void * _pTarget = NULL;


void
log_initialise( void * Target )
{
    _pTarget = Target;
    _uStart = target_getMillisecondCounter( _pTarget );
}


void
log_printMessage( LOG_LEVEL Level, const char * Message, ... )
{
    va_list stArg;
    char szMessage[LINE_MAX];
    uint32_t uTime;


    uTime = target_getMillisecondCounter( _pTarget ) - _uStart;

    va_start( stArg, Message );
    ( void ) vsnprintf( szMessage, sizeof( szMessage ), Message, stArg );
    va_end( stArg );

    fprintf( stdout, "[%lu.%03lu] %s: %s\n",
            ( unsigned long ) ( uTime / 1000 ),
            ( unsigned long ) ( uTime % 1000 ),
            _szLevel[ Level ],
            szMessage );
}
